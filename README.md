**kraken-hybride**

*Source of Kraken OS debian metapackages - HybriDE , release 2018.10.02*

# PACKAGING

* Install package :

```sh
sudo apt install build-essential devscripts debhelper fakeroot
```

* Build .deb :

```sh
git clone https://gitlab.com/kraken-sec/pkg-deb/kraken-hybride.git
mkdir kraken-hybride/kraken-hybride-2018.10.02
cp -r kraken-hybride/debian/ kraken-hybride/kraken-hybride-2018.10.02/
cd kraken-hybride/kraken-hybride-2018.10.02/
```

>amd64

```sh
debuild -i -uc -us --target-arch=amd64 --build=any
debuild -i -uc -us --build=all
```

>i386

```sh
debuild -i -uc -us --target-arch=i386 --build=any
debuild -i -uc -us --build=all
```

